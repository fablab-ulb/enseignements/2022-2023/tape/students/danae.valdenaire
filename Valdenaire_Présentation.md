# Projet Foldscope 2022/2023

## Courte présentation de Danaé Valdenaire :

Je suis étudiante en physique théorique spécialisée en physique des particules et cosmologie. Comme beaucoup de chercheurs autour du monde, je m'atèle à chercher la matière noire à l'aide du détecteur de neutrinos IceCube en Antarctique. J'ai débuté mes études dans une formation d'ingénieure et j'ai espoir que ce projet me reconcilie avec cette discipline.

## Présentation du projet :

Au cours de ce projet, nous allons étudier le **Foldscope**, microscope développé par Manu Prakash et son équipe en s'inspirant des sciences frugales. Ce microscope est la preuve que simplicité et complexité ne sont parfois pas si éloignées. D'apparence, c'est un objet on ne peut plus simple. Pourtant, il offre la **même précision qu'un microscope classique** en coûtant moins de 1$ à produire. En effet, le foldscope possède une résolution de l'ordre du submicron. En comparaison, un microscope d'1 cm de diamètre à une résolution de 0.67 micron. Sur le papier, le foldscope a une résolution tout à fait suffisante pour un usage sérieux. Nous verrons si c'est le cas au travers de différentes observations par la suite. 
On peut le dire, c'est un objet fantastique et la preuve que la science de pointe n'est pas réservée qu'à une élite restreinte de chercheurs bien loin des problèmes de la vie quotidienne. C'est d'ailleurs une des missions de la **science frugale** : **rendre la science accessible**, connectée au réel et **peu coûteuse**. La science frugale a également une dimension **écologique** en faisant aussi bien avec moins ce qui résonne tout particulièrement dans notre société où la surconsommation est un problème majeur.


## Journal de bord - 26/09 - Séance 1 :

Le but de cette séance est de prendre en main le [foldscope](https://foldscope.com/) et de se plonger dans la documentation technique pour en saisir les subtilités.

Le foldscope est un microscope ultra-low cost inspiré de l'origami qui permet plusieurs types de microscopie : la microscopie brightfield, darkfield, par fluorescence et la microscopie avec plusieurs lentilles en série. Il est composé d'une lentille sphérique facile à produire en grande quantité et d'une LED qui permet de l'utiliser sans avoir recours à une source d'éléctricité.

<div align='center'>![fold](fold et mat.jpg)</div>
<div align='center'>Foldscope avec son materiel</div>
<br>

Le foldscope a été développé pour détecter la présence de la malaria chez les populations précaires grâce à une analyse sanguine, maladie responsable d'un très grand nombre de morts encore aujourd'hui. Permettre aux habitants de ces pays d'accéder à un microscope très peu coûteux et facile d'utilisation peut littéralement leur sauver la vie. 
C'est pour cette raison que le foldscope a été conçu de façon à pouvoir être produit en très grande quantité facilement et rapidement. 

Mais son utilisation ne se réduit pas au domaine médical. Il a également une grande utilité [pédagogique](https://www.pickedu.com/foldscope). Les sciences semblent souvent effrayantes et hors de portée pour bon nombre de personnes, enfants ou adultes. La création d'instruments tel que le foldscope permet d'une part, d'éveiller la curiosité scientifique des enfants dans des pays en développement (mais pas que), et d'autre part de désacraliser les sciences dures en les présentant comme accessibles et attrayantes. Le foldscope est utilisé dans des domaines plus étonnant comme l'agriculture, le machine-learning, la biodiversité, et j'en passe. Chacun est libre de l'utiliser à sa guise, sa seule limite étant sa créativité. Toutes les publications concernant le foldscope peuvent se trouver ici : https://foldscope.com/pages/publications. 
On peut noter **quelques aspects remarquables du foldscope** : 

- **Lentille sphérique** : La micro-lentille sphérique du foldscope est un petit bijou de technologie. D'une part elle permet d'obtenir une magnification similaire à un microscope classique soit un grossissement de 2000 avec une résolution de l'ordre du submicron. D'autre part, elle peut être produite rapidement, facilement et pour un coût extrêmement abordable (0,56$ pour la magnification la plus élevée). De plus, le fait qu'elle soit incrustée dans un élément de plastique la rend totalement résistante au choc, faisant du foldscope un objet complexe technologiquement mais quasi-incassable. 

- **Eveil de la curiosité & pédagogie** : Le foldscope est un merveilleux outil de communication scientifique. Grâce à son faible coût, sa facilité de production et sa portabilité, il peut être distribué en masse dans des pays du monde entier peu importe leur situtation économique. Selon moi, plus l'accès aux outils scientifiques sera simple plus l'attrait pour les sciences grandira. La connaissance acquise avec ce type d'outil, pédagogique et bien pensé, permet de casser le mythe que les sciences sont difficiles d'accès et réservée à un groupe restreint de personnes. De plus, le foldscope est designé de façon à pouvoir être emmené partout et n'est pas réservée qu'à l'enceinte d'une salle de classe. Les élèves peuvent l'amener chez eux, le prendre en main, faire des observations de tout ce qui se trouve dans leur environnement. Le foldscope permet d'éveiller leur curiosité, d'être dans une démarche autonome et également de les reconnecter au monde qui les entoure. Ces caractéristiques font du foldscope un objet très puissant, qui ouvre la voie vers une nouvelle façon d'appréhender les sciences.

- **Facilité de montage et d'utilisation** : Une des ces forces pour moi est la facilité de prise en main de cet outil. Tous ces composants tiennent sur une feuille A4, prédécoupé et orné d'un code couleur qui a l'avantage d'être universel. Le procédé de montage est ludique et ne dure que quelques minutes. Le foldscope s'accroche facilement à l'objectif de nimporte quel smartphone et permet de prendre en photo ses observations très facilement. Il fonctionne complètement sans électricité, ni batterie. L'utilisation du foldscope avec un smartphone m'a rappelé l'univers du jeu vidéo, ce qui rend son utilisation très agréable.

Dans notre cas, nous allons embarquer le foldscope sur une bouée qui permettra de tester la qualité de l'eau de mer dans la région de la Havane. Pour ce faire, nous allons tester le foldscope et déterminer comment l'utiliser pour détecter des phytoplanctons et des microparticules de plastique qui sont tout deux des indicateurs de qualité et de pollution de l'eau. Nous allons nous concentrer ici sur la faisabilité de ce projet et l'étude préliminaire. Mon travail avec le foldscope ne consiste pas à effectuer ce projet dans son intégralité mais à m'en servir comme une sorte de fil rouge. Pour pouvoir être efficace et productive dans un projet j'ai besoin de voir où il est censé aboutir. Le fait d'avoir un projet plus global dans lequel mon travail s'insère m'a permis de bien m'organiser et de visualiser ce vers quoi je devais aboutir. 

### Montage du foldscope 

Nous avons monté le foldscope en classe avec mon binôme [Simon](@simon.biot). Nous n'avons pas rencontré de difficulté particulière, la notice est très claire et synthétique. Pour ma part, j'ai surtout eu peur d'abimer une des pièces en les décrochant de la feuille support. Il faut prendre son temps et être un minimum concentré mais je n'ai pas trouvé que l'étape du montage était particulièrement compliquée.
Pour prendre en main le foldscope, nous devons faire une observation avec un objet de notre quotidien.  
J'ai choisi d'observer un pelure d'oignon qui est une des premières observations que j'ai faite au microscope à l'école. Premièrement, il faut préparer la lamelle et le dispositif expérimental composé du foldscope évidemment et de mon téléphone portable pour prendre en photo les observations. 

### Protocole expérimental : observation d'une coupe d'oignon rouge

J'ai choisi de travailler avec des lamelles en verre car je les trouve plus pratique à utiliser. Pour préparer une lamelle il faut bien entendu une lamelle, du scotch, l'objet que vous souhaitez observer, le folscope et votre téléphone portable. Je commence par nettoyer ma lamelle au savon pour enlever tout résidu. Ensuite je coupe un petit bout d'oignon et je prélève une couche de peau interne à l'aide de la pince à épiler fournie dans le kit foldscope et je la place au centre de la lamelle. Enfin, je place un morceau de scotch sur la peau d'oignon et ma lamelle est prête pour l'observation.
Je la place dans le foldscope. Je place la LED sur la foldscope. J'attache l'embout aimanté à l'aide de scotch sur l'objectif de mon téléphone en prenant garde de bien les aligner. Enfin j'aimante le foldscope sur mon téléphone et le tour est joué. Je suis prête à observer.

NB :  Pour une meilleure efficacité de magnification, on peut placer quelques gouttes d'huile sur la lamelle avant de placer le scotch, je ne l'ai pas fait ici car je l'ai appris après mais je tiens tout de même à le préciser.

<div align='center'>![Oignon](oignon.jpg)
Coupe d'oignon rouge vue au foldscope</div> 

Cette observation m'a donné une idée. Voici 3 images d'aliments du quotidien prises à l'aide du foldscope. **Saurez-vous deviner qui est quoi ?**

<div align='center'>![QUIZ](quiz.jpg)
Quiz : réponse à la fin de la séance 2 </div> 

## 03/10 - Séance 2 :
### Exemple d'utilisation du foldscope dans le milieu scientifique 

Le foldscope trouve son utilité dans différents domaines scientifiques mais pas que. On le retrouve dans de nombreuses applications médicales qui était son but premier, mais il peut également s'avérer très utile dans des domaines comme l'éducation, la biodiversité, l'agriculture ect. Nous allons passer en revue quelques unes de ces applications. 

- [**Biodiversité : étude génétique des souris lémurien**](https://watermark.silverchair.com/genetics0651.pdf?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAAvYwggLyBgkqhkiG9w0BBwagggLjMIIC3wIBADCCAtgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM7nA3O97ogkpi59HhAgEQgIICqT0759f0G3TSe8nxlDzurWe0-2Ut6NyROtfaQaSBDlIznzs4oCkRcj9Wvohx1VQMeMrn5WeBAA7I482YXX-nVl-aNqct5V7ZM1dZcu__ONrmIuUrymgLudCNAkhz-Q_OHWbNQ-HdtWQAcgIIitS37tdrlx5DL5BJd8baIFUMMtO1HkeznemxVuBl1Q37N90qgzTjEc764KW2WfpUoDarAmNqi0XMGEhR9CkKXttqZpsVAGcAUp59qrMqUX_oeM-shlX4SvYz2xavlcpTLzZZKQ7nERbHC6jDrJMwd0a33x6fryBsZV5ws1pOL74y6jQO7jFFkVoOCOJMg1VuILERHMIaGAl_HVhRTJMt90WBlJkCjstjFtLmM7Eihe9w6v1P4kpe1Qvku53pn28FF-NXLxEHcu7y3s91dfjHFAYPvVi8Z-Jh_XoomDnYRrTQHUrOa1ITxAOz0w43tgjJwuRpHzwBXZvV6lSTLp2_c4YmaQAOKmAizL2Kf4tpA3OsFBzEHm-JRzVFO9b2CkSEXKO-5_0Jl0a6-tN4Mj6NB2-uG4Mmv-De0hP7ulvVKbQWpIiFfKk0SLwxu56bzlujVBUJgW_YhIFKY8hB5QTZFiJnArIxnRK_upLrTnhibuWFgXG-Z0h12J-ZNP2C6b2UbLy6qJgGDAKOg9IZrpc8t5KKxYRugzhGiG5Gs-KIPOQzLgQkbWpmId7g355dSIqRNz2putgb_4DKHrE6uYiZsT7KjHFPPYFIRZGGDgFpYV5R3RVz-ZlWgZC6dVGn_LRHTp5zBbvyLGs2Q2PnpSaZS3sU72SL79x9Uw8J7MqdC4o1jYDSxyU29_KemYS3SS-TqX9wV8HzEduXcUyt02vPPYoiccieUnj_mXCNbvFMTiNlvhxcFnXLvrqqSMuRLQ)
    - Les souris lémuriens font partie des primates les plus abondants sur Terre. Ajouté à cela leur cycle de reproduction très court et leur petite taille, ils sont des candidats de choix pour étudier les comportements, les gènes et les maladies des primates. Leur phylogénie, physiologie et leur comportement ont été vivement étudiés dans des laboratoires européens mais également au sein de leur habitat dans la forêt tropicale malgache. Pour rappel, la [phylogénie](https://fr.wikipedia.org/wiki/Phylog%C3%A9nie) consiste à établier les liens de parenté entre les différentes espèces terrestres, en dressant un arbre phylogénique.

<div align='center'>![Phylogenic tree](Phylogenic tree-min.PNG)
Arbre phylogénique entre différents primates et vertébrés</div> 

Plus les espèces ont de gènes en commun, plus elles sont proches dans l'arbre phylogénétique. 
La [physiologie](https://fr.wikipedia.org/wiki/Physiologie) consiste à étudier le rôle, le fonctionnement et l'organisation mécanique, physique et biochimique des organismes vivants et de leurs composants. Il s'agit donc ici de dresser une étude génétique et biologique des souris lémurien, que l'on peut généraliser (dans la mesure du possible) à tous les primates. Une étude biologique d'une espèce consiste à dresser tous les signes distinctifs que l'on peut observer pour identifier les allèles des différents gènes. Par exemple, des variations de la couleurs des yeux, du poil ect. Quand ces caractères distinctifs sont identifiés, on peut dresser le profil génétique de l'espèce. Dans cette étude, les chercheurs ont identifié plus de 50 gènes pour les souris lémuriens. Ils ont récupéré certains individus pour effectuer un examen vétérinaire complet. Un échantillon de peau au niveau de l'oreille est prélevé afin de fournir une source renouvelable de cellule et d'ADN. L'observation de ces cultures de cellules se fait en temps normal à l'aide d'un microscope mais peut aussi s'effectuer à l'aide d'un foldscope. Des images d'observations ne sont pas fournies dans ce papier.

- [**Histopathologie à l'aide du folscope**](https://sumathipublications.com/index.php/ijcbr/article/view/194/226) : étude de pathologies à partir de tissus d'animaux ou de plantes. On entend ici par tissus un groupe de cellules qui ont la même fonction. Différents types de tissus qui travaillent ensemble forment un organe.  
    - Imagerie de différents échantillons histopathologiques à l'aide du foldscope et comparaison avec un microscope classique. Les chercheurs ont conclu que la morphologie des tissus peut être déterminée grâce au foldscope avec une **précision comparable** à celle d'une microscope classique. C'est ce paramètre qui a été utilisé pour comparer les deux outils.
L'étude a été faite sur des tissus de grenouille : sang (figure 1), artère (figure 2) et os respectivement (figure 3). Il est intéressant de noter que rien ne vient contredire le fait d'obtenir des résultats du même niveau de précision sur des tissus humains. 

<div align='center'>![Histopathology frog blood](histopathologie_blood.PNG)

![Histopathology frog artery](histopathologie_artery.PNG)

![Histopathology frog bone](histopathologie_bone.PNG)</div>
 
Ces images ont été prises grâce à un téléphone portable installé directement sur le foldscope comme on le voit sur les figures 4 et 5. 

<div align='center'>![Installation 1](Smartphone et foldscope.PNG)

![Installation 2](Smartphone et foldscope 2.PNG)</div>

Toutes les figures ci-dessus proviennent de l'article : https://sumathipublications.com/index.php/ijcbr/article/view/194/226.

- [**Dépistage du cancer du col de l'utérus grâce à un examen cytologique**](https://bmcwomenshealth.biomedcentral.com/articles/10.1186/s12905-020-00902-0)   
    - La [cytologie](https://www.hopkinsmedicine.org/health/treatment-tests-and-therapies/cytology) est l'examen d'un type de cellule, le plus souvent dans un fluide, très utilisée dans la détection de cancers par exemple. Ici, on cherche à déterminer s'il est possible de détecter des cancers du col de l'utérus à l'aide du foldscope en comparant les résultats obtenus à l'aide d'un foldscope à ceux obtenus avec un microscope classique. Les chercheurs ont utilisé 4 types différents d'échantillons cytologiques du col (obtenus par frottis) : 10 normaux, 10 avec lésions malpighiennes intra-épithiale de bas grade, 10 de haut-grade et 10 frottis malins. Les cellules malpighiennes sont les cellules qui recouvrent le col de l'utérus. 
Des lésions malpighiennes intra-épithiale de bas grade ([LSIL](https://en.wikipedia.org/wiki/Bethesda_system#LSIL)) signifie que certaines cellules malpighiennes présentent des anomalies, elles peuvent se résorber d'elles-mêmes ou évoluer en cancer du col. 

<div align='center'>![LSIL](Cytopathology_of_low-grade_squamous_intraepithelial_lesion_600_400.png)
Source : Wikipédia</div>

Quant aux lésions malpighiennes intra-épithiale de haut grade ([HSIL](https://en.wikipedia.org/wiki/Bethesda_system#HSIL)), elles sont causées par le papillomavirus (HPV), une maladie sexuellement transmissible. Les cellules infectées présentent un schéma de développement anormal appelé [dysplasie](https://fr.wikipedia.org/wiki/Dysplasie).

<div align='center'>![HSIL](High-grade_squamous_intraepithelial_lesion_400_400.jpg)
HSIL - Source : Wikipédia</div>

Comme précedemment, les chercheurs ont comparé les résultats obtenus avec un foldscope et un microscope en déterminant qualitativement la sensibilité, la précision et la spécificité du foldscope. 
Pour ce faire, ils ont utilisé le [coefficient kappa pondéré](https://en.wikipedia.org/wiki/Cohen%27s_kappa#Weighted_kappa) qui est un indicateur statistique qui permet d'évaluer la fiabilité inter-évaluateurs (ou intra-évaluateurs). Autrement dit, c'est un indice d'accord entre deux items qualitatifs, qui prend en compte que l'accord peut être dû au hasard contrairement à un simple pourcentage d'accord. Ici, les items qualitatifs sont les observations faites au foldscope et au microscope. Il est à noter que l'usage de ce coefficient ne fait pas l'unanimité en raison de la difficulté d'interprétation des indices d'accord. Ils ont obtenu un coefficient kappa d'une valeur de 0.68 soit 68%, ce qui correspond à une bonne fiabilité des résultats obtenus avec le foldscope.

A travers ces études, on voit que le foldscope est un outil fiable scientifiquement et qui permet tout à fait de mener une étude scientifique rigoureuse. On peut se demander néanmoins si son utilisation ne pourrait pas être plus adaptée à un cadre de recherche. Peut-il être encore plus performant et quels outils peut-on développer pour faciliter son utilisation dans un cadre "professionel" ? C'est justement ce sur quoi porte le travail d'[Arthur](@arthur.robert).

Réponse quiz : A. Feuille de basilic, B. Chair de poivron rouge, C. Curry 

## 10/10 - Séance 3 :

Pendant cette séance, nous avons passé en revue les différentes utilisations possibles du foldscope que chacun avait étudié. Par exemple, [Nasra](@nasra.daher) nous a parlé de l"utilisation du foldscope pour détecter les contrefaçons de safran. [Arthur](@arthur.robert) nous a expliqué comment utiliser le foldscope pour étudier des cellules souches. Enfin, [Simon](@simon.biot) nous a présenté comment le foldscope s'insère dans une dynamique d'auto-diagnostic qui peut s'avérer très utiles pour des populations isolées.

La seconde partie de la séance portait sur la prise en main du foldscope en faisant des observations de façon autonome. Nous avons observé sur une lamelle de verre déjà faite puis nous avons créée nos propres échantillons sur des lamelles papier pour effectuer les observations. Nous avons observer une aiguille de cactus, une patte, une aile et un oeil de mouche puis un morceau de peau morte (aucune souffrance animale n'a été requise pour cette expérience). Pour ce faire, nous avons fixé le foldscope sur l'objectif d'un téléphone puis nous avons pris des photos des différents échantillons.

<div align='center'>![Obs classe](observations_3.jpg)</div>

<div align='center'> Observations au foldscope faites en classe : A. épines de cactus, B. patte de mouche, C. peau morte</div>
<br>
Quelques difficultées techniques ont été rencontrées :

- **L'alignement** de la lentille avec l'objectif du téléphone se fait difficilement car il faut fixer un aimant sur l'objectif du téléphone avec du scotch ce qui n'est pas très précis.
- Il n'y a **pas de support** pour le téléphone donc les réglages du foldscope pendant une observation doivent se faire forcément à 4 mains.
- La focalisation avec la pièce papier qui gère le déplacement en z n'est pas très précis car trop souple. Il n'est pas évident d'obtenir une image nette. 
- Les **lames papier** n'étaient pas stables et glissaient pendant l'observation. Il a fallu en insérer plusieurs en même temps comme c'est spécifié dans le guide car le foldscope a été conçu pour être utilisé avec des lames de verre, plus épaisses qu'une lame papier. 
- Le foldscope est censé être utilisable avec une simple **lumière naturelle** mais les observations sont bien plus nettes avec la lumière LED fournie dans le kit. 

Dans un second temps, nous avons établi les missions de chacun. Pour ma part, il s'agit de faire des **observations de phytoplanctons** car c'est un indicateur utile pour **évaluer la qualité de l'eau**. 

## 17/10 - Séance 4 : 

Pour observer du phytoplancton, je suis allée récupérer un échantillon aux étangs d'Ixelles.

<div align='center'>![Bocal](bocal.jpg)
Bocal contenant l'eau de l'étang</div>

Mon objectif a également été mieux défini. Il s'agissait dans un premier temps de déterminer comment étudier la qualité de l'eau à partir d'observations de microplastiques et de phytoplanctons. Mais ce sujet étant trop vaste pour le reste du temps imparti, j'ai décidé de me concentrer uniquement sur le phytoplancton. L'idée est d'observer du phytoplancton, de les classer suivant leur type, d'évaluer leur taille ect.

Je vais faire l'observation sur un échantillon passé dans une [centrifugeuse frugale](https://web.stanford.edu/group/prakash-lab/cgi-bin/labsite/wp-content/uploads/2017/12/Paperfuge.pdf) fabriquée par mes soins. Il est nécessaire de centrifuger l'échantillon pour accélérer la sédimentation afin de récolter un maximum de dépôt dans l'échantillon.

Avant toute chose, qu'est-ce que du [phytoplancton](https://fr.wikipedia.org/wiki/Phytoplancton) ?
Le phytoplancton est un organisme obtenant son énergie par photosynthèse.Pour rappel, la photosynthèse est un processus biologique qui opère grâce à la lumière. Les organismes vont utiliser l'énergie de la lumière du soleil pour transformer le $C0_2$ du milieu dans lequel ils se trouvent en $0_2$. La plupart de ces organismes ne sont pas visibles à l'oeil nu, mais en quantité suffisante, ils apparaissent comme une coloration à la surface de l'eau. 

<div align='center'>![Phytoplanctons coloration](320px-Phytoplankton_Bloom.jpg)
Coloration de la mer provoquée par le phytoplancton</div>
<br>

Le phytoplancton est facilement différentiable du zooplancton par des formes très simples (pas de pattes, pas d'antennes) souvent géométriques (rond, carré, ovale...). Il existe deux grandes catégories de phytoplanctons : les protistes et les bactéries. 

- Les **[protistes](https://fr.wikipedia.org/wiki/Protista)** sont des eucaryotes, à organisation cellulaire simple, unicellulaire ou multicellulaire mais sans tissu spécialisé, comme les plantes ou les champignons. Drôle de coïncidence, certains biologistes présentent les protistes comme les constituants de "la matière noire de la vie". C'est un groupe du vivant très diversifié, on y retrouve près de 20 000 espèces différentes. Dans cette catégorie, on retrouve les diatomées, organismes unicellulaires dont la taille varie de 2 microns à 1mm. Ils participent à 50% de la production primaire océanique globale, ce qui signifie qu'ils sont un élément essentiel de la chaîne alimentaire océanique.

<div align='center'>![diatomées](diatomées.PNG)</div>
<div align='center'> Source : Wikipédia </div>
<br>

- Les **[bactéries](https://fr.wikipedia.org/wiki/Bact%C3%A9rie)** sont des organismes microscopiques le plus souvent unicellulaires, ou parfois pluricellulaires, présents dans tous les milieux. On retrouve spécifiquement les **cyanobactéries** qui obtiennent leur énergie par photosynthèse et qui donne cette couleur verte à l'eau dans laquelle elles se trouvent. Dans les cyanobactéries, on trouve le plus petit organisme connu capable de faire la photosynthèse : c'est le *Prochlorococcus* et il mesure seulement 0.5 à 0.8 $\mu$m.

<div align='center'>![cyano](cyano_wiki.jpg)</div>
<div align='center'> Fiche de gauche : Cyanobactéries unicellulaires seules ou en colonie ; Fiche de droite : Filaments de cyanobactéries simples. Le repère fait environ 10$\mu$m Source : Wikipédia </div>
<br>

### Protocole expérimental : Préparation de la lamelle et centrifugation

<div align='center'>![materiel](materiel_foldscope.jpg)</div>
<div align='center'>A. Lamelle et pipette, B. Lamelle finalisée, C. Echantillon d'eau dans une grosse cuillère </div>
<br>
J'ai tout d'abord travaillé avec une centrifugeuse frugale faite en fil de pêche et de cartons que j'ai faite en me basant sur la [documentation](https://web.stanford.edu/group/prakash-lab/cgi-bin/labsite/wp-content/uploads/2017/12/Paperfuge.pdf).

<div align='center'>![Centrifugeuse](312603811_500786755258667_7995806444479545434_n.jpg)
Centrifugeuse frugale premier essai</div>

Cependant, j'ai eu du mal à l'utiliser donc j'ai décidé d'utiliser une autre méthode, toujours dans une logique frugale. J'ai attaché l'échantillon à l'horizontale sur mon essoreuse à salade et j'ai tourné la poignée quelques secondes. 

<div align='center'>![Centrifugeuse](312157500_1194730734808821_5168405768351746922_n-min.jpg)
Centrifugeuse frugale communément appelée essoreuse à salade</div>

Il s'avère que mon échantillon n'a pas été placé dans le bon sens, ce qui a été gentiment mentionné par mes camarades. Pour ne plus se tromper, j'ai fait un petit schéma qui résume les forces qui s'exerce sur un solide en rotation. Finalement, l'essoreuse à salade n'est pas suffisante.

<div align='center'>![force centrifuge](Force centrifuge.png)</div>
<div align='center'>Forces fictives d'inertie et vitesse s'appliquant sur l'extrémité d'un solide en rotation </div>
<br>
J'ai utilisé 4 échantillons différents au total pour maximiser les chances d'observation. Il s'avère que l'observation de phytoplanctons n'est pas aussi facile que je l'aurai imaginé. Théoriquement, on devrait pouvoir en observer dans une eau provenant d'un étang car ils prolifèrent dans tous les points d'eaux, salés ou eaux douces. 

Une des difficultés provient de l'identification. Pour identifier des phytoplanctons, il faut utiliser une [clé d'identification](https://www.researchgate.net/publication/27666882_Phytoplankton_Identification_Manual)qui recense les phytoplanctons avec leur forme, leur taille, leurs caractéristique ect. 

Voici quelques unes de mes observations : 

<div align='center'>![Observations 1](observations_4.jpg)

Quelques observations de l'eau au foldscope.</div>

Les éléments que l'on observe sont très certainement des déchets organiques, des végétaux ou des poussières provenant du sol. Pour aller plus en précision, il aurait été intéressant de mesurer ces éléments. A noter que cet échantillon n'a pas été centrifugé suffisament, ce qui peut expliquer la difficulté d'observer des phytoplanctons ou d'autres éléments "intéressants" sur cette lamelle. 

## 24/10 - Séance 5 :

Cette séance m'a permis d'identifier les raisons pour lesquelles je n'ai pas encore réussi à observer du phytoplancton. Les conseils et commentaires de M. Terwagne et de mes camarades m'ont été précieux. 

Dans cette séance, nous avons réglé les problèmes de la centrifugeuse frugale appelée [paperfuge](https://web.stanford.edu/group/prakash-lab/cgi-bin/labsite/wp-content/uploads/2017/12/Paperfuge.pdf). Je l'ai reconstitué totalement en coupant le cercle en carton et les différents trous grâce à une découpeuse laser pour plus de précision. Deux petites pièces en plastique ont été placées au centre pour réduire les frottements de la corde sur le carton. Enfin, j'ai choisi une corde assez robuste mais souple pour que le mécanisme fonctionne correctement. En effet, pour que l'efficacité soit maximale, il faut que la corde puisse s'enrouler sans trop de contraintes. 

<div align='center'>![centrifugeuse 2](centri_2.jpg)</div>
<div align='center'>Centrifugeuse frugale $2^{ème}$ essai
</div>
<br>

Grâce à ce graphique, on peut déterminer la vitesse de rotation max du disque en fonction de sa taille. Mon disque fait 12 cm, il peut donc théoriquement atteindre les 60000 tr/min. Sachant qu'une centrifugeuse professionnelle fait environ 10000 tr/min, la vitesse atteinte par ma centrifugeuse sera largement suffisante pour sédimenter le phytoplancton dans l'eau. 

<div align='center'>![graph](RPM_diametre.PNG)</div>
<div align='center'>Diamètre du disque central en fonction de la vitesse de rotation max. Source : paperfuge
</div>
<br>
Ceci étant fait, on peut réitérer le protocole de préparation de la lamelle décrit plus tôt. On fixe le foldscope à l'objectif d'un téléphone et nous sommes prêts à observer.
Voici quelques unes des observations que j'ai effectuées : 

<div align='center'>![obs_phyto](phyto_obs.png)</div>
<div align='center'>Observations de l'eau de l'étang faites avec le foldscope</div>
<br>
On voit de nombreux organismes sur ces observations, certains colorés, d'autres non. Les organismes filamentaires font penser à un certain type de cyanobactéries illustré plus haut. Tous ces organismes ne sont pas visibles à l'oeil nu, on peut donc espérer que certains sur des phytoplanctons. Cependant, on ne peut pas conclure sur leur nature nottament à cause du protocole effectué. Vous trouverez plus de détails dans la partie "Analyse des observations".

### Mesure de phytoplanctons

Ces observations ont été faites avec un quadrillage de 0.5mm placé sur la lamelle. Lorsque cela était possible, un marqueur noir d'une longueur de 0.5mm a été placé sur les images. Un marqueur bleu a également été placé, c'est ce marqueur que nous allons mesurer. Pour effectuer cette mesure, j'ai utilisé l'outil de mesure du logiciel GIMP. Cet outil permet de mesurer une longueur dans nimporte quelle unité sur une image. 
Ma méthode a été de mesurer en nombre de pixel le marqueur noir, puis de mesurer également en nombre de pixel le marqueur bleu. Sachant que le marqueur noir fait 0.5mm, j'ai effectué un produit en croix pour déterminer la longueur du marqueur bleue en millimètre. 
<div align='center'>![obs_phyto](phyto_mes.png)</div>
<div align='center'>Observations de l'eau de l'étang faites avec le foldscope</div>
<br>
<div align='center'>![mesure](mesure.PNG)</div>
<div align='center'>Tableau de valeurs des différentes mesures de longueur faites sur mes observations</div>
<br>

## Analyse des observations

Pour analyser ces observations, j'ai contacté Nathalie Gypens qui est une biologiste marine de l'ULB. Elle a accepté de me recevoir avec un de ses doctorants et sa technicienne de laboratoire. L'information principale qui revient de cette réunion est que mes observations ne sont pas viables car le **protocole n'est pas adapté**. L'observation de phytoplanctons requiert un protocole rigoureux qui nécessite d'être effectué en **laboratoire**. 
<br>
Je vais décrire le protocole ici tel qu'il m'a été enseigné. Il est important de noter que le protocole d'observation dépend fortement du milieu étudié et aussi de la période d'observations. En effet, la concentration de phytoplanctons varie en fonction de **l'eutrophisation du milieu**, c'est à dire la concentration de nutriments dans l'eau. Plus le milieu est eutrophisé, plus le phytoplancton va proliférer ce qui augmentera les chances d'observation au microscope. La concentration de phytoplanctons dépend également de la **période d'observation**. Plus la température extérieure baisse, plus la concentration de phytoplanctons diminue. Autrement dit, il faut faire ses observations au printemps ou en été, car en hiver il y a peu voir pas de phytoplanctons dans l'eau. 

### Protocole d'observation de phytoplanctons dans un étang de Bruxelles

- Récupérer un échantillon assez conséquent (par exemple, un petit bocal) au printemps ou en été.

La suite doit se faire en laboratoire pour avoir accès à certains produits chimiques qui ne sont pas disponibles dans le commerce.

- Fixer son échantillon grâce à du formaldéhyde. Les phytoplanctons sont des êtres vivants, sans nutriments pour se nourrir ils vont mourir et se décomposer. Pour palier ce problème, on verse du formaldéhyde dans l'échantillon. Ce produit va également colorer les organismes, ce qui permettra de les mettre en lumière pendant l'observation au foldscope. Le fixage doit se faire dans les 3h après la collecte de l'échantillon.

- Placer l'échantillon dans un cylindre gradué ou un verre à pied (pas celui qu'on utilise pour l'apéro mais bien la verrerie de laboratoire). 

<div align='center'>![materiel](materiel.png)</div>
<div align='center'>A gauche : cylindres gradués. A droite : verre à pied</div>
<br>

Laisser sédimenter le contenu pendant au moins 24h. Il ne faut pas centrifuger l'échantillon car cela va casser les possibles groupements de phytoplanctons. Les phytoplanctons sont fragiles, il ne faut pas les malmener. 

- Une fois la sédimentation faite, récupérer le dépot au fond du contenant à l'aide d'une pipette.

- Placer quelques gouttes sur une lame de verre bien propre et placer un petit carré de plastique ou un morceau de scotch sur les gouttes. Votre lamelle est prête pour l'observation. 

- Enfin, placer le foldscope, préalablement équipé de la LED, sur l'objectif de votre téléphone en prenant garde à l'alignement des deux appareils et faites vos observations. 

- Pour identifier les différentes espèces que vous pouvez rencontrer, utiliser une [clé d'identification](https://www.researchgate.net/publication/27666882_Phytoplankton_Identification_Manual) de phytoplanctons.


## Conclusion du projet

Bien que le défi "Observer et identifier des espèces de phytoplanctons" n'ait pas complètement abouti, je considère que ce projet est tout de même une réussite. En effet, j'ai réussi à effectuer des observations où l'on voit différents organismes invisibles à l'oeil nu. En suivant le protocole recommandé par les biologistes marins, je pense qu'il est possible d'observer des phytoplanctons et de les identifier aussi bien qu'avec un microscope classique. Je peux donc affirmer que le foldscope rempli sa mission de microscope frugal permettant de faire des expériences aussi sérieuses qu'avec des intruments de laboratoire plus classiques. 
Je note tout de même une certaine méfiance du monde scientique à l'égard de cette science peu coûteuse. A mon sens, c'est grâce à des projets comme celui-ci et bien d'autres qu'elle se fera sa place petit à petit dans le monde professionel. 

## Enseignements

Dans un premier temps, ce projet m'a appris ce que sont les sciences frugales et leur grande utilité. Elles permettent de faire aussi bien, avec peu d'argent, peu de matériel mais avec une grande ingéniosité. 
J'ai également découvert ce que sont les phytoplanctons, en quoi ils sont un indicateur écologique utile et surtout un maillon important de la biodiversité marine. 
<br>
Evidemment il m'a fallu prendre en main le foldscope, ce qui a été assez rapide car il est très simple d'utilisation. J'ai découvert grâce à la documentation du foldscope tous les types de microscopie qui existent et leur spécificité. Je me suis également refamiliarisée avec les sciences de la Vie théorique et expérimentale. Théorique à travers mon étude bibliographique autour du foldscope et mes recherches sur les phytoplanctons. Expérimentale à travers mes observations personnelles et le protocole qui encadre mon projet.
<br>
Ce projet a été un challenge car l'observation de phytoplanctons pour une étudiante en physique n'est pas si simple. Il m'aura fallu de la détermination et du travail pour mener ce projet à terme. D'une certaine façon, j'ai appris à gérer l'innatendu et à m'adapter à l'évolution de ma guideline durant ces quelques semaines.
<br>
Enfin, j'ai appris à maitriser gitlab et le language markdown que je ne connaissais pas du tout. j'ai également appris à utiliser le logiciel gimp que je connaissais simplement de nom. J'ai trouvé ce projet très enrichissant de part sa différence évidente avec mes études et le type de travaux qu'on me demande en général. J'ai beaucoup apprécié l'aspect collaboratif de ce cours, et l'aspect Do It Yourself qui n'est pas très présent dans des cours plus théoriques. 

## Outils et logiciels utilisés

- Logiciel GIMP : utile pour le traitement d'images. Il permet de régler la taille des images et leur qualité. Toutes les images ont été retouchées pour utiliser un minimum de données.

- [Canva](https://www.canva.com/) : outil de design que j'ai utilisé pour faire les montages photos et les schéma.
